#!/usr/bin/python
import csv
import requests

# Upload URI
URI = "http://swg99sap32.germany.stern-wywiol-group.com:8086/write?db=opcuaHistory"
# Input File name
inputFilename = "ns=1;s=SM_E_32_IMP1.csv"  # timestamp,value
# Target measurement
targetMeasurement = "ns=1;s=SM_E_32_IMP1"
# Calculated points
insertedPoints = []
# Expected interval in s
expectedInterval = 60
# tolerance in %
tolerance = 0.1

# Import CSV
with open(inputFilename) as csvdatei:
    csv_reader_object = csv.DictReader(
        csvdatei,
        fieldnames=("timestamp", "value"),
        delimiter=",",
    )

    data = sorted(csv_reader_object, key=lambda row: (row["timestamp"]))
    # print(*data, sep="\n")
    filteredData = filter(lambda x: x["value"] != "", data)

    # Calculate points to fill
    lastTimestamp = None
    lastValue = None

    for idx, el in enumerate(filteredData):

        # print(
        #     "IDX: ",
        #     idx,
        #     " ;Timestamp: ",
        #     int(int(el["timestamp"]) / 1000),
        #     ";Value: ",
        #     el["value"],
        # )

        currentTimestamp = int(int(el["timestamp"]) / 1000)

        # no calculation on first element
        if idx > 0:
            # do we have to create points?
            if (currentTimestamp - lastTimestamp) > (
                expectedInterval + (expectedInterval * tolerance)
            ):
                pointsToCreate = (
                    (currentTimestamp - lastTimestamp) / expectedInterval
                ) - 1
                # print("Points to create: ", pointsToCreate)

                # Calculate pitch
                m = (float(el["value"]) - float(lastValue)) / (
                    currentTimestamp - lastTimestamp
                )
                # print("m = ", m)

                for x in range(int(pointsToCreate)):
                    p = {
                        "timestamp": lastTimestamp + ((x + 1) * expectedInterval),
                        "value": m * ((x + 1) * expectedInterval) + float(lastValue),
                    }
                    # print("P(", x, "): ", p)
                    insertedPoints.append(p)

        # set last
        lastTimestamp = currentTimestamp
        lastValue = el["value"]

# Upload
# print("Calculated points:")
# print(*insertedPoints, sep="\n")
line = ""
for idx, el in enumerate(insertedPoints):
    # Append point
    line += (
        targetMeasurement
        + " value="
        + str(float(el["value"]))
        + " "
        + str(int(el["timestamp"]))
        + "000000000\n"
    )

    # Upload every 100 el
    if (idx + 1) % 100 == 0:
        resp = requests.post(URI, data=line)

        if resp.status_code != 204:
            print(resp.reason)
            exit(-1)

        # print("[P]Line:", line)
        line = ""

# Upload remaining
# print("[R]Line:", line)
resp = requests.post(URI, data=line)

if resp.status_code != 204:
    print(resp.reason)
    exit(-1)

print("DONE")
